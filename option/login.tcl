#!/usr/bin/expect

set HOST [lindex $argv 0]
set USER [lindex $argv 1]
set PASSWORD [lindex $argv 2]

#设置超时
set timeout 10

if {$argc < 3} {
    puts "请正确执行指令: expect $argv0 host user password"
    exit
}
#spawn 启动一个进程执行指令
spawn ssh $USER@$HOST

#对spawn进程output进行捕获
expect {
    "*yes/no*" {send "yes";expect_continue;}
    "*password:*" {send "$PASSWORD"}
}
#保持交互状态
interact
