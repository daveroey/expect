#!/usr/bin/expect

#########################################
# ①本机主机A,代理主机B,目标机器C            #
# ②A能到B,B能到C,A不能直达C. 需要使用B来代理 #
#########################################

#目标主机
set HOST [lindex $argv 0]
set USER [lindex $argv 1]
set PASSWORD [lindex $argv 2]

#代理机器主机
set PROXY_HOST 192.168.73.10
set PROXY_USER admin
set PROXY_PASSWORD admin123

#设置超时
set timeout 10

if {$argc < 3} {
    puts "请正确执行指令: expect $argv0 host user password"
    exit
}


#登录到代理机器
spawn ssh -l $PROXY_USER -p 22 $PROXY_HOST
expect {
    "*yes/no*" {send "yes\r";exp_conntinue;}
    "*password:*" {send "$PROXY_PASSWORD\r"}

}

#登录到目标机器
expect "*$PROXY_USER@*" {send "ssh -l $USER -p 22 $HOST\r";}
expect {
    "*yes/no*" {send "yes\r";expect_continue;}
    "*password:*" {send "$PASSWORD\r"}
}

#保持交互状态
interact
