# expect自动化脚本

#### 项目介绍
有些操作shell是无法完成的，可以使用expect自动化脚本,实现自动交互简化人工操作。expect脚本是基于tcl脚本的http://docs.activestate.com/activetcl/8.6/tcl/contents.html expect可以在unix/linux,windows环境运行.建议在unix/linux下运行。

### expect环境安装
1. linux下expect安装：https://www.linuxidc.com/Linux/2012-10/72761.htm
2. mac下expect安装：https://blog.csdn.net/jw_xuezhixia/article/details/54144116

#### expect小案例
1. expect实现简单自动登录https://gitee.com/daveroey/expect/blob/master/option/login.tcl
2. expect通过代理机自动登录内网https://gitee.com/daveroey/expect/blob/master/option/login_proxy.tcl
3. expect批量scp操作https://gitee.com/daveroey/expect/blob/master/option/scp.tcl
>说明：实例中的脚本后缀为tcl，为tcl语言后缀，笔者使用vscode编写。vscode具有tcl支持插件。

#### autoexpect 自动化脚本录制
    autoexpect可以记录一系列操作，然后生成一个expect脚本。生成的脚本可以直接使用，也可以加以修改实现所需的功能。
autoexpect本身也是一个expect脚本https://gitee.com/daveroey/expect/blob/master/auto/autoexpect.exp
使用前提：需要安装tcl依赖和expect环境。
使用方法：expect autoexpect.exp -p 即可开始录制。回到当前环境使用exit即可退出录制。录制完成会生成一个script.exp脚本文件。使用expect script.exp即可运行脚本文件。

#### 参考资料

1. ActiveTcl官方expect介绍：http://docs.activestate.com/activetcl/8.6/tcl/expect/expect.1.html
2. 来自互联网参考
    http://docs.activestate.com/activetcl/8.6/tcl/expect/expect.1.html
    https://blog.csdn.net/julius_lee/article/details/8060006



#### 联系作者
邮箱:389788728@qq.com
